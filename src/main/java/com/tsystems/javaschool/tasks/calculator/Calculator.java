package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        // проверка на null
        if(statement == null)
            return null;

        // избавляемся от всех пробелов
        statement = statement.replaceAll("\\s+","");

        int lengthStatment = statement.length() - 1;

        // проверка на пустую строку
        if(lengthStatment == -1)
            return null;

        // первый символ цифра, откр.скобка или минус
        // последний символ цифра или закр.скобка
        if(
            !(statement.charAt(0) >= 48 && statement.charAt(0) <= 57 || statement.charAt(0) == 40 || statement.charAt(0) == 45) ||
            !(statement.charAt(lengthStatment) >= 48 && statement.charAt(lengthStatment) <= 57 || statement.charAt(lengthStatment) == 41)
        )
            return null;

        if(!checkPoints(statement))
            return null;

        // отбираем операторы
        List<String> listOperators = getOperators(statement);
        if(listOperators == null)
            return null;

        // отбираем операнды
        List<String> listOperands = getOperands (statement);
        if(listOperands == null)
            return null;

        Double decision = calculate(listOperators, listOperands);
        if(decision == null)
            return null;

        return new DecimalFormat("#.####").format(decision).replace(',', '.');
    }

    /**
     * Проверить на повторяющиеся точки или не точки
     * @param statement первоначальная сторока
     * @return false - есть ошибка в statement, true - ошибок нет
     */
    private boolean checkPoints(String statement){
        // отбираем все символы - не операторы или цифры
        String[] points = statement.split("[()+\\-/*\\d]+");
        // проверка на повторяющиеся точки или не точки
        if(points.length > 1)
            for (String symbol:points) {
                if(!symbol.isEmpty() && symbol.equalsIgnoreCase(".") == false)
                    return false;
            }
        return true;
    }

    /**
     * Получить коллекцию из операторов и проверить синтаксис <br> ОТВЕТ ПРОВЕРИТЬ НА NULL !!!
     * @param statement первоначальная сторока
     * @return null - если есть ошибки касающиеся операторов,<br>
     *         List<String> - если все в норме
     */
    private List<String> getOperators(String statement){
        // отбираем операторы
        String operators[] = statement.split("[\\d\\.]+");

        // проверяем наличие у скобок примыкающих операторов
        for (int i = 0; i < operators.length; i++) {
            if(operators[i].length() >1 && operators[i].equalsIgnoreCase("") == false){
                if(i != 0 && operators[i].indexOf('(') != -1 && !operators[i].matches("[+\\-/*]?[(]+"))
                    return null;
                else if(i == 0 && operators[i].indexOf('(') != -1 && !operators[i].matches("[(]+"))
                    return null;
                else if(i != operators.length - 1 && operators[i].indexOf(')') != -1 && !operators[i].matches("[)]+[+\\-/*]"))
                    return null;
                else if(i == operators.length - 1 && operators[i].indexOf(')') != -1 && !operators[i].matches("[)]+[+\\-/*]?"))
                    return null;
                    // повторение подряд скобок допустимо, но не остальных операторов!!!
                else if(operators[i].indexOf(')') == -1 && operators[i].indexOf('(') == -1)
                    return null;
            }

        }

        operators = String.join("", operators).split("");
        List<String> listOperators = new ArrayList<>(Arrays.asList(operators));

        // каунтеры на кол-во открывающихся/закрывающихся скобок
        int cntOpenBracke = 0,
                cntCloseBracket = 0;

        // должны быть только допущенные символы
        // + считаем кол-во открывающихся/закрывающихся скобок
        for (int i = 0; i < listOperators.size(); i++) {
            switch (listOperators.get(i)){
                case "+":
                case "-":
                case "*":
                case "/":
                    break;
                case "(":
                    cntOpenBracke++;
                    break;
                case ")":
                    cntCloseBracket++;
                    break;
                default :
                    return null;
            }
        }
        // должно быть: кол-во открывающихся скобок == кол-во закрывающихся
        if(cntOpenBracke != cntCloseBracket)
            return null;

        return listOperators;
    }

    /**
     * Получить коллекцию из операндов и проверить синтаксис <br> ОТВЕТ ПРОВЕРИТЬ НА NULL !!!
     * @param statement первоначальная сторока
     * @return null - если есть ошибки касающиеся операндов,<br>
     *         List<String> - если все в норме
     */
    private List<String> getOperands(String statement){
        String operands[] = statement.split("[()+\\-/*]+");
        List<String> listOperands = new ArrayList<>(Arrays.asList(operands));
        for(int i = 0; i < listOperands.size(); i++){
            if(!listOperands.get(i).matches("-?\\d+[\\.\\d]?\\d*"))
                return null;
        }
        return listOperands;
    }

    /** Расчитываем <br> ВНИМАНИЕ - все проверки синтаксиса в getOperators() и getOperands() <br> ОТВЕТ ПРОВЕРИТЬ НА NULL !!!
     * @param listOperators получаем из getOperators()
     * @param listOperands получаем из getOperands()
     * @return Double - по выполнении всех операций<br>
     *         null - если пытаемся разделить на ноль
     */
    private Double calculate(List<String> listOperators, List<String> listOperands){

        // развесовка операторов для верной последовательности исполнения
        Map<String, Integer> weightOperators = new HashMap<>();
        weightOperators.put("+", 1);
        weightOperators.put("-", 1);
        weightOperators.put("*", 2);
        weightOperators.put("/", 2);
        weightOperators.put("(", 3);
        weightOperators.put(")", 4);

        // номер исполняемого оператора
        int numberDoOperator = 0;
        // вес исполняемого оператора
        int weightDoOperator = 0;
        // модификатор веса операции если она в скобках
        int modifierWeightBracket = 0;
        // номер исполняемого операнда
        int numberDoOperand = 0;

        while (true){
            numberDoOperator = 0;
            weightDoOperator = 0;
            modifierWeightBracket = 0;

            // ищем самую тяжелую операцию с правого края
            for (int i = 0; i < listOperators.size(); i++) {
                if(weightOperators.get(listOperators.get(i)) + modifierWeightBracket > weightDoOperator){
                    // дошли до закрытия скобок, дальше смотреть пока нет смысла
                    if(weightOperators.get(listOperators.get(i)) == 4)
                        break;

                    numberDoOperator = i;
                    // вес оператора состоит из его собственного и модификатора скобок
                    weightDoOperator = weightOperators.get(listOperators.get(i)) + modifierWeightBracket;

                    if(weightOperators.get(listOperators.get(i)) == 3)
                        modifierWeightBracket +=3;
                }
            }

            // если число просто находится внутри скобок без каких либо операторов
            if(weightDoOperator % 3 == 0){
                listOperators.remove(numberDoOperator);
                listOperators.remove(numberDoOperator);
                continue;
            }

            // ищем номер нужного оператора
            // если операция внутри скобок
            if(weightDoOperator > 3) {
                numberDoOperand = numberDoOperator - weightDoOperator / 3;
            }else{
                numberDoOperand = numberDoOperator;
            }

            // выполняем операцию
            switch (listOperators.get(numberDoOperator)){
                case "+":
                    listOperands.set(
                            numberDoOperand,
                            String.valueOf(Double.valueOf(listOperands.get(numberDoOperand)) + Double.valueOf(listOperands.get(numberDoOperand + 1))));
                    break;
                case "-":
                    listOperands.set(
                            numberDoOperand,
                            String.valueOf(Double.valueOf(listOperands.get(numberDoOperand)) - Double.valueOf(listOperands.get(numberDoOperand + 1))));
                    break;
                case "*":
                    listOperands.set(
                            numberDoOperand,
                            String.valueOf(Double.valueOf(listOperands.get(numberDoOperand)) * Double.valueOf(listOperands.get(numberDoOperand + 1))));
                    break;
                case "/":
                    if(Double.valueOf(listOperands.get(numberDoOperand + 1)) != 0)
                        listOperands.set(
                                numberDoOperand,
                                String.valueOf(Double.valueOf(listOperands.get(numberDoOperand)) / Double.valueOf(listOperands.get(numberDoOperand + 1))));
                    else return null;
                    break;
            }
            listOperands.remove(numberDoOperand + 1);
            listOperators.remove(numberDoOperator);

            if(listOperators.size() == 0)
                break;
        }
        return Double.valueOf(listOperands.get(0));
    }
}