package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        // кол-во переданных элементов
        double sizeInputNumbers = inputNumbers.size();

        // отметаем коллекции где есть нулы
        if(inputNumbers.contains(null))
            throw new CannotBuildPyramidException();

        // пирамида - это числовая прогрессия n+1
        // cумма первых n членов (n+1)*n/2
        // (CountRows^2+CountRows)/2 = sizeInputNumbers
        // решаем обычное квадратное уравнение
        Double countRows = (-1 + Math.sqrt(1+8*sizeInputNumbers))/2;

        // кол-во строк может быть только целым
        if(countRows - countRows.intValue() != 0)
            throw new CannotBuildPyramidException();

        //P.S. countRows = кол-ву символов (без нулей) на нижней гране
        // кол-во нулей = countRows - 1
        int countCollumns = (int) (countRows*2 - 1);

        // сортируем
        inputNumbers.sort(Comparator.naturalOrder());

        int[][] pyramid = new int[countRows.intValue()][countCollumns];

        // заполняем пирамиду
        int startPosition = countCollumns/2;
        int counter = 0;
        for (int rowNumber = 0; rowNumber < countRows ; rowNumber++, startPosition --)
            for(int counterCell = 0; counterCell <= rowNumber; counterCell++)
                pyramid[rowNumber][startPosition + counterCell*2] = inputNumbers.get(counter++);

        return pyramid;
    }


}
