package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here

        if(y == null || x == null)
            throw new IllegalArgumentException();

        // из y удаляем элементы, не принадлежащие x
        y.retainAll(x);
        int sizeX = x.size();

        // если размер у < x, то точно нельзя составить подпоследовательность
        if(y.size() < sizeX)
            return false;

        int counter = 0;
        Object lastObject = null;
        while (sizeX > counter){
            // последовательность в норме
            if(x.get(counter).equals(y.get(counter))){
                lastObject = x.get(counter);
                counter++;
            }else{
                // последовательность НЕ в норме - смотрим сбился ли порядок
                if(lastObject != null && y.get(counter).equals(lastObject))
                    y.remove(counter);
                else
                    return false;
            }
        }
        return true;
    }
}
